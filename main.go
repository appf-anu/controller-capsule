package main

import (
	"encoding/json"
	"flag"
	"fmt"
	ctools "gitlab.com/appf-anu/chamber-tools"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

var (
	errLog *log.Logger
)

var (
	noMetrics, dummy, loopFirstDay, monitorExtended bool
	address                                         string
	conditionsPath                                  string
	section                                         string
	authToken                                       string
	monitorURL, infoURL, setURL                     *url.URL
	tags                                            ctools.TelegrafTags
	interval                                        time.Duration
)

func indexInSlice(a string, list []string) int {
	for i, b := range list {
		if strings.Trim(b, "\t ,\n") == a {
			return i
		}
	}
	return -1
}

func getMonitorResponseFromDevice() (*monitorResponse, error) {
	v := new(monitorResponse)

	netClient := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest("GET", monitorURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("X-Auth-Token", authToken)

	resp, err := netClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &v)
	if err != nil {
		return nil, err
	}
	return v, nil
}

func getInfoResponseFromDevice(v *infoResponse) error {
	//v := newInfoResponsePtr()
	netClient := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest("GET", infoURL.String(), nil)
	if err != nil {
		return err
	}
	req.Header.Add("X-Auth-Token", authToken)

	resp, err := netClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &v)
	if err != nil {
		return err
	}
	return nil
}

//// calls setvar with name=<name>&value=<value>
func setVarOnDevice(name string, value string) error {
	netClient := &http.Client{
		Timeout: time.Second * 10,
	}
	q := setURL.Query()
	q.Set("name", name)
	q.Set("value", value)
	setURL.RawQuery = q.Encode()
	req, err := http.NewRequest("POST", setURL.String(), nil)
	if err != nil {
		return err
	}
	req.Header.Add("X-Auth-Token", authToken)

	resp, err := netClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
        return fmt.Errorf("HTTP response code not 200: %d", resp.StatusCode)
    }
	return nil
}

// runStuff, should send values and write metrics.
// returns true if program should continue, false if program should retry
func runStuff(point *ctools.TimePoint) bool {
	info := newInfoResponsePtr()
	if point.Temperature != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("T_Set", fmt.Sprintf("%.1f", point.Temperature)); err != nil {
			errLog.Println(err)
			return false
		}
		info.TemperatureTarget = point.Temperature

	}
	if point.RelativeHumidity != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("RH_Set", fmt.Sprintf("%d", int(point.RelativeHumidity))); err != nil {
			errLog.Println(err)
			return false
		}
		info.RelativeHumidityTarget = int64(point.RelativeHumidity)

	}
	if point.CoolWhite != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("CoolWhite_Set", fmt.Sprintf("%.3f", point.CoolWhite)); err != nil {
			errLog.Println(err)
			return false
		}
		info.CoolWhiteTarget = point.CoolWhite

	}
	if point.DeepRed != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("DeepRed_Set", fmt.Sprintf("%.3f", point.DeepRed)); err != nil {
			errLog.Println(err)
			return false
		}
		info.DeepRedTarget = point.DeepRed

	}
	if point.FarRed != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("FarRed_Set", fmt.Sprintf("%.3f", point.FarRed)); err != nil {
			errLog.Println(err)
			return false
		}
		info.FarRedTarget = point.FarRed

	}
	if point.RoyalBlue != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("RoyalBlue_Set", fmt.Sprintf("%.3f", point.RoyalBlue)); err != nil {
			errLog.Println(err)
			return false
		}
		info.RoyalBlueTarget = point.RoyalBlue

	}
	if point.Blue != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("Blue_Set", fmt.Sprintf("%.3f", point.Blue)); err != nil {
			errLog.Println(err)
			return false
		}
		info.BlueTarget = point.Blue

	}
	if point.Cyan != ctools.NullTargetFloat64 {
		if err := setVarOnDevice("Cyan_Set", fmt.Sprintf("%.3f", point.Cyan)); err != nil {
			errLog.Println(err)
			return false
		}
		info.CyanTarget = point.Cyan
	}

	err := getInfoResponseFromDevice(info)
	if err != nil {
		errLog.Println(err)
		return true
	}

	fmt.Println(ctools.DecodeStructToLineProtocol("capsule", tags, info))
	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("capsule", tags, info); err != nil {
			errLog.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}
	ev := ctools.GetEnvironmentalStats(info.Temperature, float64(info.RelativeHumidity))
	fmt.Println(ctools.DecodeStructToLineProtocol("capsule", tags, ev))
	for x := 0; x < 5; x++ {
		if err := ctools.WriteMetricUDP("capsule", tags, ev); err != nil {
			errLog.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		break
	}

	return true
}

func init() {
	var err error
	hostname := os.Getenv("NAME")
	errLog = log.New(os.Stderr, "[capsule] ", log.Ldate|log.Ltime|log.Lshortfile)

	// get the local zone and offset
	//flag.Usage = usage
	flag.BoolVar(&noMetrics, "no-metrics", false, "dont collect metrics")
	if tempV := strings.ToLower(os.Getenv("NO_METRICS")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			noMetrics = true
		} else {
			noMetrics = false
		}
	}

	errLog.Println()

	flag.BoolVar(&dummy, "dummy", false, "dont send conditions to chamber")
	if tempV := strings.ToLower(os.Getenv("DUMMY")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			dummy = true
		} else {
			dummy = false
		}
	}

	flag.BoolVar(&loopFirstDay, "loop", false, "loop over the first day")
	if tempV := strings.ToLower(os.Getenv("LOOP")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			loopFirstDay = true
		} else {
			loopFirstDay = false
		}
	}

	flag.BoolVar(&monitorExtended, "monitor-extended", false, "monitor extended values")
	if tempV := strings.ToLower(os.Getenv("MONITOR_EXTENDED")); tempV != "" {
		if tempV == "true" || tempV == "1" {
			monitorExtended = true
		} else {
			monitorExtended = false
		}
	}

	flag.StringVar(&tags.Host, "host-tag", hostname, "host tag to add to the measurements")
	if tempV := os.Getenv("HOST_TAG"); tempV != "" {
		tags.Host = tempV
	}

	flag.StringVar(&tags.Group, "group-tag", "nonspc", "host tag to add to the measurements")
	if tempV := os.Getenv("GROUP_TAG"); tempV != "" {
		tags.Group = tempV
	}

	flag.StringVar(&tags.DataID, "did-tag", "", "data id tag")
	if tempV := os.Getenv("DID_TAG"); tempV != "" {
		tags.DataID = tempV
	}

	flag.StringVar(&conditionsPath, "conditions", "", "conditions file to")
	if tempV := os.Getenv("CONDITIONS_FILE"); tempV != "" {
		conditionsPath = tempV
	}
	flag.DurationVar(&interval, "interval", time.Minute*10, "interval to run conditions/record metrics at")
	if tempV := os.Getenv("INTERVAL"); tempV != "" {
		interval, err = time.ParseDuration(tempV)
		if err != nil {
			errLog.Println("Couldnt parse interval from environment")
			errLog.Println(err)
		}
	}

	flag.StringVar(&section, "section", "", "section to run")
	if tempV := os.Getenv("SECTION"); tempV != "" {
		section = tempV
	}

	flag.StringVar(&authToken, "auth-token", "", "auth token to pass to 'X-Auth-Token")
	if tempV := os.Getenv("AUTH_TOKEN"); tempV != "" {
		authToken = tempV
	}

	flag.Parse()

	if noMetrics && dummy {
		errLog.Println("dummy and no-metrics specified, nothing to do.")
		os.Exit(1)
	}
	if conditionsPath != "" && !dummy {
		if tags.Group == "nonspc" {
			tags.Group = "spc"
		}
	}

	//check if address is provided by
	if address = os.Getenv("ADDRESS"); address == "" {
		address = flag.Arg(0)
	}

	ip := net.ParseIP(address)
	if ip != nil {
		address = fmt.Sprintf("http://%s/", ip)
	}
	u, err := url.Parse(address)
	if err != nil {
		panic(err)
	}
	u.Scheme = "http"
	u.Path = ""
	//u.RawQuery = ""
	fmt.Println(u.String())
	monitorURL, err = url.Parse(u.String())
	if err != nil {
		panic(err)
	}
	monitorURL.Path = fmt.Sprintf("%s/rest/monitor", section)

	infoURL, err = url.Parse(u.String())
	if err != nil {
		panic(err)
	}
	infoURL.Path = fmt.Sprintf("%s/rest/info", section)

	setURL, err = url.Parse(u.String())
	if err != nil {
		panic(err)
	}
	setURL.Path = fmt.Sprintf("%s/rest/setvar", section)
	setURL.RawQuery = ""

	errLog.Printf("noMetrics: \t%t\n", noMetrics)
	errLog.Printf("dummy: \t%t\n", dummy)
	errLog.Printf("loopFirstDay: \t%t\n", loopFirstDay)
	errLog.Printf("timezone: \t%s\n", ctools.ZoneName)

	errLog.Printf("tags: %+v", tags)
	errLog.Printf("address: \t%s\n", address)
	errLog.Printf("infoURL: \t%s\n", infoURL.String())
	errLog.Printf("setURL: \t%s\n", setURL.String())
	errLog.Printf("monitorURL: \t%s\n", monitorURL.String())

	errLog.Printf("file: \t%s\n", conditionsPath)
	errLog.Printf("interval: \t%s\n", interval)

}

func main() {
	if interval == time.Second*0 {
		info := newInfoResponsePtr()
		err := getInfoResponseFromDevice(info)
		if err != nil {
			errLog.Println(err)
			return
		}
		fmt.Println(ctools.DecodeStructToLineProtocol("capsule", tags, info))
		if monitorExtended {
			info, err := getMonitorResponseFromDevice()
			if err != nil {
				errLog.Println(err)
				return
			}
			fmt.Println(ctools.DecodeStructToLineProtocol("capsule_ext", tags, info))
		}
		ev := ctools.GetEnvironmentalStats(info.Temperature, float64(info.RelativeHumidity))
		fmt.Println(ctools.DecodeStructToLineProtocol("capsule", tags, ev))
		os.Exit(0)
	}

	if !noMetrics && (conditionsPath == "" || dummy) {
		runMetrics := func() {
			info := newInfoResponsePtr()
			err := getInfoResponseFromDevice(info)
			if err != nil {
				errLog.Println(err)
				return
			}
			fmt.Println(ctools.DecodeStructToLineProtocol("capsule", tags, info))
			for x := 0; x < 5; x++ {
				if err := ctools.WriteMetricUDP("capsule", tags, info); err != nil {
					errLog.Println(err)
					time.Sleep(200 * time.Millisecond)
					continue
				}
				break
			}
			ev := ctools.GetEnvironmentalStats(info.Temperature, float64(info.RelativeHumidity))
			fmt.Println(ctools.DecodeStructToLineProtocol("capsule", tags, ev))
			for x := 0; x < 5; x++ {
				if err := ctools.WriteMetricUDP("capsule", tags, ev); err != nil {
					errLog.Println(err)
					time.Sleep(200 * time.Millisecond)
					continue
				}
				break
			}
			if monitorExtended {
				info, err := getMonitorResponseFromDevice()
				if err != nil {
					errLog.Println(err)
					return
				}
				fmt.Println(ctools.DecodeStructToLineProtocol("capsule_ext", tags, info))
				for x := 0; x < 5; x++ {
					if err := ctools.WriteMetricUDP("capsule_ext", tags, info); err != nil {
						errLog.Println(err)
						time.Sleep(200 * time.Millisecond)
						continue
					}
					break
				}
			}
		}

		runMetrics()

		ticker := time.NewTicker(interval)
		go func() {
			for range ticker.C {
				runMetrics()
			}
		}()
		select {}
	}

	if conditionsPath != "" && !dummy {
		ctools.RunConditions(errLog, runStuff, conditionsPath, loopFirstDay)
	}

}
