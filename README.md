# controller-capsule
* ![master:docker-build](https://gitlab.com/appf-anu/controller-capsule/badges/master/pipeline.svg?job=build_master) *master:docker-build*

* ![master:go-lint](https://gitlab.com/appf-anu/controller-capsule/badges/master/pipeline.svg?job=golint) *master:go-lint*

* ![master:go-staticcheck](https://gitlab.com/appf-anu/controller-capsule/badges/master/pipeline.svg?job=staticcheck) *master:go-staticcheck*

* ![master:go_vet](https://gitlab.com/appf-anu/controller-capsule/badges/master/pipeline.svg?job=go_vet) *master:go-vet*

* ![master:go_test](https://gitlab.com/appf-anu/controller-capsule/badges/master/pipeline.svg?job=go_test) *master:go-test*

* ![master:go_build](https://gitlab.com/appf-anu/controller-capsule/badges/master/pipeline.svg?job=go_build) *master:go-build*


PSI fytotron capsule controller, uses the PSI rest api.

Controls and gathers data from the PSI fytotron capsule.

## Usage

### Options
```
--auth-token: The authorization token to pass to 'X-Auth-Token’ (mandatory for access).
--conditions: Path of the capsule conditions input file.
--loop: Loop over the conditions for the first day, ignoring the rest of the conditions file.
--section: The section of the conditions file to run.
--interval: The interval for recording device metrics, in minutes. If not provided, the default interval is 10 minutes.
--no-metrics: Don’t collect device metrics.
--monitor-extended: Collect the extra metrics that capsules provide (which you may or may not find useful).
For a list, see: https://gitlab.com/appf-anu/controller-capsule/-/blob/master/monitorResponse.go
--dummy: A “dummy run” - don’t send conditions to the chamber.

The following tags can be used to group the device metrics:
--host-tag: name/ID of capsule
--group-tag: chamber group (e.g. spc, nonspc)
--did-tag: data ID - e.g. experiment/trial ID
