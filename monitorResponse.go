package main

import (
	"encoding/json"
	"fmt"
	"reflect"
)

// monitorResponse struct to hold data from endpoint 172.23.0.10:8000/section1/rest/monitor
type monitorResponse struct {
	PLCCommunication         string `caption:"PLC Comunication"`
	CabinetVentilatorBreaker string `caption:"Cabinet ventilator breaker"`
	CoolingWaterExtremeLow   string `caption:"Cooling water extreme low"`
	CoolingWaterLow          string `caption:"Cooling water low"`
	CoolingUnit              string `caption:"Cooling unit"`
	HumidifierUnit           string `caption:"Humidifier unit"`
	DehumidifierUnit         string `caption:"Dehumidifier unit"`
	PowerLost                string `caption:"Power lost"`
	PowerRestored            string `caption:"Power restored"`
	LightsAirFilter          string `caption:"Lights air fitler"`
	MainPump                 string `caption:"Main pump"`
	HallEnvironment          string `caption:"Hall environment"`
	OutsideEnvironment       string `caption:"Outside environment"`
	RecuperationUnit         string `caption:"Recuperation unit"`
	CO2Sensor                string `caption:"CO2 sensor"`
	CoolingWaterTemperature  string `caption:"Cooling water temperature"`
	DehumidifierBreaker      string `caption:"Dehumidifier breaker"`
	CoolingUnitBreaker       string `caption:"Cooling unit breaker"`
	GSMPresent               string `caption:"GSM present"`

	// FreshAirInput            string `caption:"Fresh air input"`
	// FreshAirOutput           string `caption:"Fresh air output"`
	// F1Overheat              string `caption:""`
	// F1TemperatureOverlimit  string `caption:""`
	// F1TemperatureUnderlimit string `caption:""`
	// F1HumidityOverlimit     string `caption:""`
	// F1HumidityUnderlimit    string `caption:""`
	// F1LightsOff             string `caption:""`
	// F1Pump                  string `caption:""`
	// F1Heating               string `caption:""`
	// F1LightsBreaker         string `caption:""`
	// F1Ventilator            string `caption:""`
	// F1Environment           string `caption:""`
	// F1TemperatureRegulation string `caption:""`
	// F1HumidityRegulation    string `caption:""`

	// F2Overheat              string `caption:""`
	// F2TemperatureOverlimit  string `caption:""`
	// F2TemperatureUnderlimit string `caption:""`
	// F2HumidityOverlimit     string `caption:""`
	// F2HumidityUnderlimit    string `caption:""`
	// F2LightsOff             string `caption:""`
	// F2Pump                  string `caption:""`
	// F2Heating               string `caption:""`
	// F2LightsBreaker         string `caption:""`
	// F2Ventilator            string `caption:""`
	// F2Environment           string `caption:""`
	// F2TemperatureRegulation string `caption:""`
	// F2HumidityRegulation    string `caption:""`
}

// UnmarshalJSON unmarshals a json response from the capsule into a monitorResponse
func (mr *monitorResponse) UnmarshalJSON(data []byte) error {
	// unmarshal all the items to arbitrary interface because we get a bare json array of objects
	// with mixed types
	items := make([]interface{}, 0)
	err := json.Unmarshal(data, &items)
	if err != nil{
		return err
	}
	v := reflect.ValueOf(mr)

	if v.Kind() != reflect.Ptr || v.IsNil() {
		return fmt.Errorf("decode requires non-nil pointer")
	}
	// get the value that the pointer v points to.
	v = v.Elem()
	t := v.Type()
	for _, itInterface := range items {
		it, ok := itInterface.(map[string]interface{})
		if !ok {
			continue
		}

		for i := 0; i < v.NumField(); i++ {
			ft := t.Field(i)
			fv := v.Field(i)
			captionTag, ok := ft.Tag.Lookup("caption")
			if !ok {
				continue
			}
			if captionTag == it["Caption"] {
				valueString := it["State"]
				message := it["Message"]
				if valueString != "ok"{
					if message == ""{
						message = "No message"
					}
					valueString = fmt.Sprintf("%s: %s", valueString, message)
				}
				fv.SetString(fmt.Sprint(valueString))
			}
		}
	}

	return nil
}