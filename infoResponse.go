package main

import (
	"encoding/json"
	"fmt"
	ctools "gitlab.com/appf-anu/chamber-tools"
	"reflect"
	"strconv"
)

// infoResponse struct to hold data from endpoint 172.23.0.10:8000/section1/rest/info
type infoResponse struct {
	CO2                      float64 `name:"CO2_Actual"`
	Temperature              float64 `name:"T_Actual"`
	TemperatureSetPoint      float64 `name:"T_Set"`
	TemperatureTarget        float64
	RelativeHumidity         int64 `name:"RH_Actual"`
	RelativeHumiditySetPoint int64 `name:"RH_Set"`
	RelativeHumidityTarget   int64
	CoolWhiteSetPoint        float64 `name:"CoolWhite_Set"`
	CoolWhiteTarget          float64
	DeepRedSetPoint          float64 `name:"DeepRed_Set"`
	DeepRedTarget            float64
	FarRedSetPoint           float64 `name:"FarRed_Set"`
	FarRedTarget             float64
	RoyalBlueSetPoint        float64 `name:"RoyalBlue_Set"`
	RoyalBlueTarget          float64
	BlueSetPoint             float64 `name:"Blue_Set"`
	BlueTarget               float64
	CyanSetPoint             float64 `name:"Cyan_Set"`
	CyanTarget               float64
}

// NewInfoResponsePtr creates a new infoResponse and returns the pointer to it
func newInfoResponsePtr() *infoResponse {
	ir := &infoResponse{
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetInt64,
		ctools.NullTargetInt64,
		ctools.NullTargetInt64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
		ctools.NullTargetFloat64,
	}
	return ir
}

// UnmarshalJSON unmarshalls a json array into an infoResponse struct
func (ir *infoResponse) UnmarshalJSON(data []byte) error {
	// unmarshal all the items to arbitrary interface because we get a bare json array of objects
	// with mixed types
	items := make([]interface{}, 0)
	err := json.Unmarshal(data, &items)
	if err != nil {
		return err
	}

	v := reflect.ValueOf(ir)
	if v.Kind() != reflect.Ptr || v.IsNil() {
		return fmt.Errorf("decode requires non-nil pointer")
	}
	// get the value that the pointer v points to.
	v = v.Elem()
	t := v.Type()
	for _, itInterface := range items {
		it, ok := itInterface.(map[string]interface{})
		if !ok {
			continue
		}

		for i := 0; i < v.NumField(); i++ {
			ft := t.Field(i)
			fv := v.Field(i)
			nameTag, ok := ft.Tag.Lookup("name")

			if !ok {
				continue
			}

			if nameTag == it["Name"] {
				switch fv.Kind() {
				case reflect.Int, reflect.Int64:
					strValue := fmt.Sprint(it["Value"])
					value, err := strconv.ParseInt(strValue, 10, 64)
					if err != nil {
						continue
					}
					if multiplierString, ok := ft.Tag.Lookup("multiplier"); ok {
						if mult, err := strconv.ParseFloat(multiplierString, 64); err == nil {
							// cast to float64 and  then do the division and cast back to int64.
							floatValue := float64(value)
							floatValue /= mult
							value = int64(floatValue)
						}
					}
					fv.SetInt(value)
				case reflect.Float64, reflect.Float32:
					strValue := fmt.Sprint(it["Value"])
					value, err := strconv.ParseFloat(strValue, 64)
					if err != nil {
						continue
					}
					if multiplierString, ok := ft.Tag.Lookup("multiplier"); ok {
						if mult, err := strconv.ParseFloat(multiplierString, 64); err == nil {
							value /= mult
						}
					}
					fv.SetFloat(value)
				case reflect.String:
					fv.SetString(fmt.Sprint(it["Value"]))
				case reflect.Bool:
					strValue := fmt.Sprint(it["Value"])
					fv.SetBool(indexInSlice(strValue, []string{"ok", "true", "yes"}) >= 0)
				}
			}
		}
	}
	return nil
}
